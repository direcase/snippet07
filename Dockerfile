FROM golang:1.15.6

RUN mkdir -p /usr/src/app/

WORKDIR /usr/src/app/

COPY . /usr/src/app/

RUN go build -o Dariya main.go handlers.go helpers.go routes.go snippets.go models.go

CMD ["./Dariya"]